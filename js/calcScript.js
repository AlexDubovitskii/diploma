$(function () {

    let totalWhey = 0;
    let totalFat = 0;
    let totalCarbo = 0;
    let totalCalories = 0;


    $(".addToDairy").on("click", function (event) {
        event.preventDefault();

        $.ajax({
            url : "addToDairy",
            type: "POST",
            data: {
                totalWhey    : totalWhey,
                totalFat     : totalFat,
                totalCarbo   : totalCarbo,
                totalCalories: totalCalories
            },

        });

    });


    $(".showNutrition").on("click", function (event) {
        event.preventDefault();

        let product = $(".calc-product").val();
        let productWeight = $(".calc-product-weight").val();


        $.ajax({
            url    : "showNutrition",
            type   : "POST",
            data   : {
                product      : product,
                productWeight: productWeight
            },
            success: function (response) {
                let data = JSON.parse(response);


                totalWhey += parseFloat(data["whey"]);
                totalFat += parseFloat(data["fat"]);
                totalCarbo += parseFloat(data["carbo"]);
                totalCalories += parseFloat(data["calories"]);


                let html = "";
                let totalHtml = "" +
                    "Итого Белков " + totalWhey + "<br>" +
                    "Итого Жиров " + totalFat + "<br>" +
                    "Итого Углеводов " + totalCarbo + "<br>" +
                    "Итого Калорий " + totalCalories + "<br>";

                $(".totalResults").html(totalHtml);


                html += "<tr>" +

                    "<td>" + product + "</td>" +
                    "<td >" + data["whey"] + "</td>" +
                    "<td>" + data["fat"] + "</td>" +
                    "<td>" + data["carbo"] + "</td>" +
                    "<td>" + data["calories"] + "</td>" +
                    "</tr>";


                $(".out").append(html);


            }
        })

    });


});