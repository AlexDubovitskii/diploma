$(function () {
    init();
});

function init() {
    validateSignUp();
}

function checkField(stringToCheck, pattern) {
    return stringToCheck.match(pattern) !== null;
}

function validateSignUp() {
    let elements = document.getElementById("signUpForm").elements;
    for (let element of elements) {

        let nameAttrValue = element["name"];
        let siButton = $(".su_submit");
        let signUpOut = $(".signUpOut");

        $("[name = '" + nameAttrValue + "']").on("change", function () {
            let fieldValue = $("[name = '" + nameAttrValue + "']").val();

            switch (nameAttrValue) {
                case "su_email":
                    if (!checkField(fieldValue, /[a-z0-9\._%\+\-]+@[a-z0-9\.-]+\.[a-z]{2,}/)) {
                        siButton.prop('disabled', true);
                        signUpOut.html(nameAttrValue + " is not correct");
                    } else {
                        signUpOut.html("");
                        siButton.prop('disabled', false);
                    }
                    break;
                case "su_login":
                    if (!checkField(fieldValue, /^[A-Za-z0-9_]+$/)) {
                        siButton.prop('disabled', true);
                        signUpOut.html(nameAttrValue + " is not correct");
                    } else {
                        signUpOut.html("");
                        siButton.prop('disabled', false);

                    }
                    break;
                case "su_password":
                    if (!checkField(fieldValue, /^\S*(?=\S{8,30})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[_\-\+#$])\S*$/)) {
                        siButton.prop('disabled', true);
                        signUpOut.html(nameAttrValue + " is not correct");
                    } else {
                        signUpOut.html("");
                        siButton.prop('disabled', false);

                    }
                    break;
                case "su_password2":
                    let password = $("[name = 'su_password']").val();
                    let password2 = $("[name = 'su_password2']").val();

                    if (password !== password2) {
                        siButton.prop('disabled', true);
                        signUpOut.html(nameAttrValue + " is not correct");
                    } else {
                        signUpOut.html("");
                        siButton.prop('disabled', false);
                    }
                    break;
            }
        })
    }
}
