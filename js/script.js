//**jQuery effects**//

//nav-bar toggle effect (small devices)
$(document).ready(function () {
    $('#sidebar-btn').click(function () {
        $('#sidebar').toggleClass('visible');

    });
});

//modal window blur effect on page "recepties" add recept
$(".add-recept").on("click", function (event) {
    event.preventDefault();
    $(".blur").css({"filter": "blur(5px)"});

});

$(".md-close,.md-overlay").on("click", function (event) {
    event.preventDefault();
    $(".blur").css({"filter": "none"});

});






