<?php
/**
 * Created by PhpStorm.
 * User: alexd
 * Date: 08.06.2018
 * Time: 14:25
 */

namespace server\controllers;


use server\dev\Render as Render;

class PageController {

    public function showCabinet() {

        Render::renderTemplate("cabinet");
    }

    public static function receptiesShow() {

        Render::renderTemplate("recepties");
    }

    public static function showProducts() {

        Render::renderTemplate("products");
    }
}