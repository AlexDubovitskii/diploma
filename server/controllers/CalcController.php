<?php


namespace server\controllers;


use server\dev\Helper as Helper;
use server\dev\Render as Render;
use server\models\CalcModel as CalcModel;
use server\models\DairyModel;

class CalcController {

    public function showCalc() {

        Render::renderTemplate("calc_food");
    }

    public function calcNutrition() {

        $totalWhey = htmlspecialchars($_POST["totalWhey"]);
        $totalFat = htmlspecialchars($_POST["totalFat"]);
        $totalCarbo = htmlspecialchars($_POST["totalCarbo"]);
        $totalCalories = htmlspecialchars($_POST["totalCalories"]);

        $today = date("Y-m-d");

        DairyModel::addToDairy("whey", $totalWhey, $today);
        DairyModel::addToDairy("fat", $totalFat, $today);
        DairyModel::addToDairy("carbo", $totalCarbo, $today);
        DairyModel::addToDairy("calories", $totalCalories, $today);

    }

    public function showNutrition() {

        $product = htmlspecialchars($_POST["product"]);
        $productWeight = htmlspecialchars($_POST["productWeight"]);
        $productInfo = CalcModel::getProductInfo($product);

        $whey = round($productInfo["whey"] / 100 * $productWeight, 2);
        $fat = round($productInfo["fat"] / 100 * $productWeight, 2);
        $carbo = round($productInfo["carbo"] / 100 * $productWeight, 2);
        $calories = round($productInfo["calories"] / 100 * $productWeight, 2);

        echo json_encode(compact("whey", "fat", "carbo", "calories"));

    }

}