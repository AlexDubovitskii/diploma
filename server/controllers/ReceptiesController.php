<?php


namespace server\controllers;


use server\dev\Render as Render;

class ReceptiesController {

    public static function receptiesShow(){
        Render::renderTemplate("recepties");
    }

    public static function showSignUpForm(){
        Render::renderTemplate("login_form");
    }

}