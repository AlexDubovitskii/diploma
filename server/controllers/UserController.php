<?php


namespace server\controllers;


use server\dev\Helper;
use server\dev\Render as Render;
use server\models\UserModel as UserModel;

class UserController {

    public static function showSignUpForm() {

        Render::renderTemplate("login_form");
    }

    public static function signUp() {

        $su_login = htmlspecialchars($_POST["su_login"]);
        $su_email = htmlspecialchars($_POST["su_email"]);;
        $su_password = htmlspecialchars($_POST["su_password"]);


        if (UserModel::checkUserExistsByRow("login", $su_login)) {
            $message = "User exists";
            Helper::print($message);
        } else {
            UserModel::createUser($su_login, $su_password, $su_email);

            self::fillSession($su_login);

            header('Location:http://localhost/Diploma/cabinet');

        }
    }

    public static function signIn() {

        $si_login = htmlspecialchars($_POST["si_login"]);
        $si_password = htmlspecialchars($_POST["si_password"]);


        $hash = UserModel::getHashedPassword($si_login);

        if (UserModel::checkUserExistsByRow("login", $si_login)
            &&
            password_verify($si_password, $hash)) {

            self::fillSession($si_login);

            header('Location:http://localhost/Diploma/cabinet');

        } else {
            echo "Такого юзера нет";

        }
    }


    private static function createToken() {

        $chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";
        $charArray = str_split($chars);
        $token = "";

        for ($i = 0; $i < 15; $i++) {
            $token .= $charArray[rand(0, count($charArray) - 1)];
        }

        return $token;

    }

    public static function checkUserLogIn() {

        if (isset($_SESSION) && isset($_SESSION["token"])) {
            if ($_SESSION["token"] == self::getTokenFromDB($_SESSION["login"])) {

                return true;

            } else {
                return false;
            }
        }

        return false;
    }

    public function logOut() {

        unset($_SESSION["token"]);
        unset($_SESSION["login"]);

        header('Location:http://localhost/Diploma/signup');

    }

    private static function fillSession($login) {

        $token = self::createToken();
        $_SESSION["token"] = $token;
        $_SESSION["login"] = $login;

        UserModel::saveTokenToDB($login, $token);
    }

    private static function getTokenFromDB($login) {

        return UserModel::getTokenFromDB($login);
    }


}
