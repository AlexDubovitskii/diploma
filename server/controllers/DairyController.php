<?php
/**
 * Created by PhpStorm.
 * User: alexd
 * Date: 09.06.2018
 * Time: 16:47
 */

namespace server\controllers;


use server\models\DairyModel;

class DairyController
{

    public function showDairy()
    {
       echo json_encode(DairyModel::showDairy());
    }

}