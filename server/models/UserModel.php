<?php

namespace server\models;

use server\database\DbConnection as DbConnection;
use server\dev\Helper;

class UserModel {

    public static function createUser($login, $password, $email) {

        $connection = DbConnection::getInstance()->getDbConnection();
        $password = password_hash($password, PASSWORD_DEFAULT);

        $query = "insert into users (login, password, email) values ('$login', '$password', '$email')";

        mysqli_query($connection, $query);

    }

    public static function checkUserExistsByRow($row, $value) {

        $connection = DbConnection::getInstance()->getDbConnection();

        $query = "select count(*) as `count` from users where $row= '$value'";
        $result = mysqli_query($connection, $query);
        $userCount = mysqli_fetch_array($result);

        return (int)$userCount["count"] > 0;
    }

    public static function getHashedPassword($login) {

        $connection = DbConnection::getInstance()->getDbConnection();

        $query = "select password from users where login = '$login'";
        $result = mysqli_query($connection, $query);
        $arr = mysqli_fetch_assoc($result);

        return $arr["password"];

    }

    public static function saveTokenToDB($login, $token) {

        $connection = DbConnection::getInstance()->getDbConnection();

        $query = "update users set  token='$token' where login = '$login'";

        mysqli_query($connection, $query);
    }

    public static function getTokenFromDB($login) {

        $connection = DbConnection::getInstance()->getDbConnection();

        $query = "select token from users where login='$login'";

        $result = mysqli_fetch_assoc(mysqli_query($connection, $query));

        return $result["token"];
    }


}