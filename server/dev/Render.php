<?php

namespace server\dev;

class Render {

    public static function renderTemplate($page, $data = []) {


        extract($data);
        $page .= '.html';
        if (file_exists("html/" . $page)) {
            include_once "html/" . $page;
        }
    }


}
