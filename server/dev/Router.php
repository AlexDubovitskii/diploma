<?php


namespace server\dev;


use server\controllers\SignUpController;
use server\controllers\UserController;
use server\dev\Helper as Helper;

class Router {

    public static $routes = [];
    public static $route = [];


    public static function addRoute($url, $controller, $action) {

        $arr = array("controller" => $controller, "action" => $action);

        self::$routes[$url] = $arr;
    }

    public static function getRoutes() {

        return self::$routes;
    }

    public static function getRoute() {

        return self::$route;
    }

//*******************************************************
    private static function doAction($url) {

        foreach (self::$routes as $index => $route) {

            if ($url == $index) {

                $class = strtolower($route["controller"]);
                $action = strtolower($route["action"]);

                if (class_exists("server\controllers\\" . $class)) {
                    $class = "server\controllers\\" . $class;
                    $controller = new $class;

                    if (method_exists($controller, $action)) {
                        $controller->$action();
                    } else {
                        echo "Метод " . $action . " в классе " . $class . " не найден";
                    }
                } else {
                    echo "Данного класса не существует";
                }
            }

        }
    }

    public static function dispatch($url) {

//        preg_match_all("#[a-z]+$#", $url, $matches);
//
//        foreach ($matches as $index => $value) {
//            $url = $value[0];
//        }


        switch ($url) {
            case "signup":
                SignUpController::showSignUpForm();
                break;
            case "signupusers":
            case "signinusers":
            case "logout":
                self::doAction($url);
                break;
            default:
                if (UserController::checkUserLogIn()) {
                    self::doAction($url);
                    break;
                }
                break;
        }


    }

}