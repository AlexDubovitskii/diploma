<?php

namespace server\database;


final class DbConnection {


    private $dbConnection;
    private static $instance;
    private $host = "localhost";
    private $user = "root";
    private $password = "root";
    private $database = "healthyfooddb";

    public static function getInstance() {

        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {

        $this->dbConnection = new \mysqli(
            $this->host,
            $this->user,
            $this->password,
            $this->database);


        if (mysqli_connect_error()) {
            trigger_error("Failed to connect to MySQL: " . mysqli_connect_error(), E_USER_ERROR);

        }
    }

    public function getDbConnection(): \mysqli {

        return $this->dbConnection;
    }

    private function __clone() {
    }

    public function __set($name, $value) {
    }

    private function __sleep() {
    }

    private function __wakeup() {
    }


}