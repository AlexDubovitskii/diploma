<?php


use server\dev\Helper as Helper;
use server\database\DbConnection as DbConnection;
use server\dev\Router as Router;
use server\models\UserModel as UserModel;

session_start();


error_reporting(-1);

define("ROOT", __DIR__);

require_once "routes.php";


$url = $_SERVER["QUERY_STRING"];

Router::dispatch($url);

