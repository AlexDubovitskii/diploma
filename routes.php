<?php

use server\dev\Router as Router;

spl_autoload_register(function ($class) {

    $path = ROOT . DIRECTORY_SEPARATOR . $class . '.php';
    $path = strtolower($path);
    $path = str_replace('\\', '/', $path);
    if (file_exists($path)) {
        require_once $path;
    }
});

Router::addRoute("signup", "UserController", "showSignUpForm");
Router::addRoute("logout", "UserController", "logout");
Router::addRoute("signupusers", "UserController", "signUp");
Router::addRoute("signinusers", "UserController", "signin");
Router::addRoute("calc", "CalcController", "showCalc");
Router::addRoute("addToDairy", "CalcController", "calcNutrition");
Router::addRoute("showNutrition", "CalcController", "showNutrition");
Router::addRoute("cabinet/showdairy", "DairyController", "showDairy");
Router::addRoute("cabinet", "PageController", "showCabinet");
Router::addRoute("products", "PageController", "showProducts");
Router::addRoute("recepties", "PageController", "receptiesShow");
